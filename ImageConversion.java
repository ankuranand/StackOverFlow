import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;

/**
 * Created by ankur on 13/7/15.
 * The Following program will read an image file.
 * convert it into byte array, and then reuse the
 * converted byte array, and convert it back to new BufferedImage
 *
 */
public class ImageToBuf {

    public  static  void main(String... strings) throws IOException {
        byte[] imageInByte;
        //read the image
        BufferedImage originalImage = ImageIO.read(new File("/home/ankur/Pictures/BlpRb.png"));
        //convert BufferedImage to byte array
        ByteArrayOutputStream byteOutS = new ByteArrayOutputStream();
        ImageIO.write(originalImage, "png", byteOutS);
        byteOutS.flush();
        imageInByte = byteOutS.toByteArray();
        byteOutS.close();

        //convert byte array back to BufferedImage
        InputStream readedImage = new ByteArrayInputStream(imageInByte);
        BufferedImage bfImage = ImageIO.read(readedImage);
        System.out.println(bfImage);
    }
}