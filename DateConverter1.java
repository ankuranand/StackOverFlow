import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * This Example
 * Takes an input in the format YYYY/MM/DD i.e 2015/07/24
 * and converts back in the format DD-MON-YY ie 24-JUL-15
 */
public class DateConverter1 {

    public static void main(String...strings) throws ParseException {

        // define the date format in which you want to take the input
        // i.e YYYY/MM/DD correspond to yyyy/MM/dd

        DateFormat inputDateFormat = new SimpleDateFormat("yyyy/MM/dd");

        // convert your date into desired input date format
        Date inputDate = inputDateFormat.parse("2015/07/20");
        // above lines gives Parse Exception if date is UnParsable


        // define the date format in which you want to give output
        // i.e dd-MMM-yy

        DateFormat outputDateFormat = new SimpleDateFormat("dd-MMM-yy");

        // don't assign it back to date as format return back String
        String outputDate = outputDateFormat.format(inputDate);

        System.out.println(outputDate);



    }
}
